use std::collections::HashMap;

use chrono::prelude::*;
use serde::{Deserialize, Serialize};

use crate::parse_triton_recv::ParseTritonRecv;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FeatureFlag {
    pub triton_recv: String,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

impl ParseTritonRecv for FeatureFlag {
    fn parse_triton_recv(&self) -> Option<chrono::DateTime<Utc>> {
        if let Ok(triton_recv) = self.triton_recv.parse::<u128>() {
            let seconds = (triton_recv / 1_000_000_000_u128) as i64;
            let nseconds = (triton_recv % 1_000_000_000_u128) as u32;
            if let Some(naive) = DateTime::from_timestamp(seconds, nseconds) {
                return Some(naive);
            }
        }
        None
    }
}
