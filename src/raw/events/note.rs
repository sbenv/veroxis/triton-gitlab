use std::collections::HashMap;

use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use serde_enum_str::{Deserialize_enum_str, Serialize_enum_str};

use crate::parse_triton_recv::ParseTritonRecv;
use crate::raw::types::commit::Commit;
use crate::raw::types::hook_date::HookDate;
use crate::raw::types::merge_params::MergeParams;
use crate::raw::types::project::Project;
use crate::raw::types::repository::Repository;
use crate::raw::types::unknown_value::UnknownValue;
use crate::raw::types::user::User;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Note {
    pub triton_recv: String,
    pub event_type: String,
    pub merge_request: Option<MergeRequest>,
    pub object_attributes: ObjectAttributes,
    pub object_kind: String,
    pub project_id: u64,
    pub project: Project,
    pub repository: Repository,
    pub user: User,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct MergeRequest {
    pub assignee_id: Option<u64>,
    pub assignee_ids: Vec<u64>,
    pub author_id: u64,
    pub blocking_discussions_resolved: bool,
    pub created_at: HookDate,
    pub description: String,
    pub first_contribution: bool,
    pub head_pipeline_id: Option<u64>,
    pub human_time_change: Option<UnknownValue>,
    pub human_time_estimate: Option<UnknownValue>,
    pub human_total_time_spent: Option<UnknownValue>,
    pub id: u64,
    pub iid: u64,
    pub labels: Vec<UnknownValue>,
    pub last_commit: Commit,
    pub last_edited_at: Option<HookDate>,
    pub last_edited_by_id: Option<u64>,
    pub merge_commit_sha: Option<String>,
    pub merge_error: Option<UnknownValue>,
    pub merge_params: MergeParams,
    pub merge_status: String,
    pub merge_user_id: Option<u64>,
    pub merge_when_pipeline_succeeds: bool,
    pub milestone_id: Option<UnknownValue>,
    pub oldrev: Option<String>,
    pub reviewer_ids: Vec<u64>,
    pub source_branch: String,
    pub source_project_id: u64,
    pub source: Project,
    pub state_id: u64,
    pub state: String,
    pub target_branch: String,
    pub target_project_id: u64,
    pub target: Project,
    pub time_change: f64,
    pub time_estimate: f64,
    pub title: String,
    pub total_time_spent: f64,
    pub updated_at: HookDate,
    pub updated_by_id: Option<u64>,
    pub url: String,
    pub work_in_progress: bool,
    pub detailed_merge_status: Option<DetailedMergeStatus>,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ObjectAttributes {
    pub attachment: Option<UnknownValue>,
    pub author_id: u64,
    pub change_position: Option<UnknownValue>,
    pub commit_id: Option<UnknownValue>,
    pub created_at: HookDate,
    pub description: String,
    pub discussion_id: String,
    pub id: u64,
    pub line_code: Option<UnknownValue>,
    pub note: String,
    pub noteable_id: u64,
    pub noteable_type: String,
    pub original_position: Option<UnknownValue>,
    pub position: Option<UnknownValue>,
    pub project_id: u64,
    pub r#type: Option<UnknownValue>,
    pub resolved_at: Option<HookDate>,
    pub resolved_by_id: Option<UnknownValue>,
    pub resolved_by_push: Option<UnknownValue>,
    pub st_diff: Option<UnknownValue>,
    pub system: bool,
    pub updated_at: HookDate,
    pub updated_by_id: Option<UnknownValue>,
    pub url: String,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Deserialize_enum_str, Serialize_enum_str, Clone, PartialEq, Eq, Hash)]
#[serde(rename_all = "snake_case")]
pub enum DetailedMergeStatus {
    /// Merge Request is open
    CiStillRunning,
    /// Merge Request is merged
    Checking,
    /// There are open discussions
    DiscussionsNotResolved,

    #[serde(other)]
    Unknown(String),
}

impl ParseTritonRecv for Note {
    fn parse_triton_recv(&self) -> Option<chrono::DateTime<Utc>> {
        if let Ok(triton_recv) = self.triton_recv.parse::<u128>() {
            let seconds = (triton_recv / 1_000_000_000_u128) as i64;
            let nseconds = (triton_recv % 1_000_000_000_u128) as u32;
            if let Some(naive) = DateTime::from_timestamp(seconds, nseconds) {
                return Some(naive);
            }
        }
        None
    }
}
