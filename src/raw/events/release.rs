use std::collections::HashMap;

use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use serde_enum_str::{Deserialize_enum_str, Serialize_enum_str};

use crate::parse_triton_recv::ParseTritonRecv;
use crate::raw::types::commit::Commit;
use crate::raw::types::hook_date::HookDate;
use crate::raw::types::project::Project;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Release {
    pub triton_recv: String,
    pub action: Action,
    pub assets: Assets,
    pub commit: Commit,
    pub created_at: HookDate,
    pub description: String,
    pub id: u64,
    pub name: String,
    pub object_kind: String,
    pub project: Project,
    pub released_at: HookDate,
    pub tag: String,
    pub url: String,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Assets {
    pub count: u64,
    pub links: Vec<Link>,
    pub sources: Vec<Source>,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Link {
    pub external: Option<bool>,
    pub id: u64,
    pub link_type: String,
    pub name: String,
    pub url: String,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Source {
    pub format: String,
    pub url: String,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Deserialize_enum_str, Serialize_enum_str, Clone)]
#[serde(rename_all = "snake_case")]
pub enum Action {
    Create,
    Update,

    #[serde(other)]
    Unknown(String),
}

impl ParseTritonRecv for Release {
    fn parse_triton_recv(&self) -> Option<chrono::DateTime<Utc>> {
        if let Ok(triton_recv) = self.triton_recv.parse::<u128>() {
            let seconds = (triton_recv / 1_000_000_000_u128) as i64;
            let nseconds = (triton_recv % 1_000_000_000_u128) as u32;
            if let Some(naive) = DateTime::from_timestamp(seconds, nseconds) {
                return Some(naive);
            }
        }
        None
    }
}
