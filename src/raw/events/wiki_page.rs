use std::collections::HashMap;

use chrono::prelude::*;
use serde::{Deserialize, Serialize};

use crate::parse_triton_recv::ParseTritonRecv;
use crate::raw::types::project::Project;
use crate::raw::types::user::User;
use crate::raw::types::wiki::Wiki;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct WikiPage {
    pub triton_recv: String,
    object_kind: String,
    user: User,
    project: Project,
    wiki: Wiki,
    object_attributes: ObjectAttributes,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ObjectAttributes {
    content: String,
    format: String,
    message: String,
    title: String,
    slug: String,
    url: String,
    action: String,
    diff_url: String,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

impl ParseTritonRecv for WikiPage {
    fn parse_triton_recv(&self) -> Option<chrono::DateTime<Utc>> {
        if let Ok(triton_recv) = self.triton_recv.parse::<u128>() {
            let seconds = (triton_recv / 1_000_000_000_u128) as i64;
            let nseconds = (triton_recv % 1_000_000_000_u128) as u32;
            if let Some(naive) = DateTime::from_timestamp(seconds, nseconds) {
                return Some(naive);
            }
        }
        None
    }
}
