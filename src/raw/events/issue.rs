use std::collections::HashMap;

use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use serde_enum_str::{Deserialize_enum_str, Serialize_enum_str};

use crate::parse_triton_recv::ParseTritonRecv;
use crate::raw::types::change::Changes;
use crate::raw::types::hook_date::HookDate;
use crate::raw::types::label::Label;
use crate::raw::types::project::Project;
use crate::raw::types::repository::Repository;
use crate::raw::types::unknown_value::UnknownValue;
use crate::raw::types::user::User;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Issue {
    pub triton_recv: String,
    pub changes: Changes,
    pub event_type: String,
    pub labels: Vec<Label>,
    pub object_attributes: ObjectAttributes,
    pub object_kind: String,
    pub project: Project,
    pub repository: Repository,
    pub user: User,
    pub assignees: Option<Vec<User>>,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ObjectAttributes {
    pub action: Option<Action>,
    pub assignee_id: Option<u64>,
    pub assignee_ids: Vec<u64>,
    pub author_id: u64,
    pub closed_at: Option<HookDate>,
    pub confidential: bool,
    pub created_at: HookDate,
    pub description: String,
    pub discussion_locked: Option<UnknownValue>,
    pub due_date: Option<String>,
    pub duplicated_to_id: Option<UnknownValue>,
    pub escalation_status: Option<String>,
    pub human_time_change: Option<UnknownValue>,
    pub human_time_estimate: Option<UnknownValue>,
    pub human_total_time_spent: Option<UnknownValue>,
    pub id: u64,
    pub iid: u64,
    pub labels: Vec<Label>,
    pub last_edited_at: Option<HookDate>,
    pub last_edited_by_id: Option<u64>,
    pub milestone_id: Option<UnknownValue>,
    pub moved_to_id: Option<UnknownValue>,
    pub project_id: u64,
    pub relative_position: Option<u64>,
    pub severity: String,
    pub state_id: u64,
    pub state: String,
    pub time_change: u64,
    pub time_estimate: u64,
    pub title: String,
    pub total_time_spent: u64,
    pub updated_at: HookDate,
    pub updated_by_id: Option<u64>,
    pub url: String,
    pub weight: Option<UnknownValue>,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Deserialize_enum_str, Serialize_enum_str, Clone)]
#[serde(rename_all = "snake_case")]
pub enum Action {
    /// Issue was opened
    Open,
    /// Issue was closed
    Close,
    /// Issue was updated
    Update,

    #[serde(other)]
    Unknown(String),
}

impl ParseTritonRecv for Issue {
    fn parse_triton_recv(&self) -> Option<chrono::DateTime<Utc>> {
        if let Ok(triton_recv) = self.triton_recv.parse::<u128>() {
            let seconds = (triton_recv / 1_000_000_000_u128) as i64;
            let nseconds = (triton_recv % 1_000_000_000_u128) as u32;
            if let Some(naive) = DateTime::from_timestamp(seconds, nseconds) {
                return Some(naive);
            }
        }
        None
    }
}
