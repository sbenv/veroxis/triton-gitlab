use std::collections::HashMap;

use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use serde_enum_str::{Deserialize_enum_str, Serialize_enum_str};

use crate::parse_triton_recv::ParseTritonRecv;
use crate::raw::types::hook_date::HookDate;
use crate::raw::types::repository_info::RepositoryInfo;
use crate::raw::types::runner::Runner;
use crate::raw::types::unknown_value::UnknownValue;
use crate::raw::types::user::User;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Build {
    pub triton_recv: String,
    pub before_sha: String,
    pub build_allow_failure: bool,
    pub build_created_at: HookDate,
    pub build_duration: Option<f64>,
    pub build_failure_reason: String,
    pub build_finished_at: Option<HookDate>,
    pub build_id: u64,
    pub build_name: String,
    pub build_queued_duration: Option<f64>,
    pub build_stage: String,
    pub build_started_at: Option<HookDate>,
    pub build_status: BuildStep,
    pub commit: Commit,
    pub environment: Option<UnknownValue>,
    pub object_kind: String,
    pub pipeline_id: u64,
    pub project_id: u64,
    pub project_name: String,
    pub r#ref: String,
    pub repository: RepositoryInfo,
    pub runner: Option<Runner>,
    pub sha: String,
    pub tag: bool,
    pub user: User,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(
    Debug, Deserialize_enum_str, Serialize_enum_str, Clone, PartialEq, Eq, PartialOrd, Ord, Hash,
)]
#[serde(rename_all = "snake_case")]
pub enum BuildStep {
    /// waiting for runner to pick it up
    Created,
    /// job is waiting for a resource
    WaitingForResource,
    /// waiting for runner to set up environment
    Pending,
    /// job is executing
    Running,
    /// job was cancelled
    Canceled,
    /// job failed
    Failed,
    /// job finished
    Success,

    #[serde(other)]
    Unknown(String),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Commit {
    pub author_email: String,
    pub author_name: String,
    pub author_url: String,
    pub duration: Option<f64>,
    pub finished_at: Option<HookDate>,
    pub id: u64,
    pub message: String,
    pub sha: String,
    pub started_at: Option<HookDate>,
    pub status: String,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

impl ParseTritonRecv for Build {
    fn parse_triton_recv(&self) -> Option<chrono::DateTime<Utc>> {
        if let Ok(triton_recv) = self.triton_recv.parse::<u128>() {
            let seconds = (triton_recv / 1_000_000_000_u128) as i64;
            let nseconds = (triton_recv % 1_000_000_000_u128) as u32;
            if let Some(naive) = DateTime::from_timestamp(seconds, nseconds) {
                return Some(naive);
            }
        }
        None
    }
}
