use std::collections::HashMap;

use chrono::prelude::*;
use serde::{Deserialize, Serialize};

use crate::parse_triton_recv::ParseTritonRecv;
use crate::raw::types::commit::Commit;
use crate::raw::types::project::Project;
use crate::raw::types::repository_info::RepositoryInfo;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Push {
    pub triton_recv: String,
    pub after: String,
    pub before: String,
    pub checkout_sha: Option<String>,
    pub commits: Vec<Commit>,
    pub event_name: String,
    pub message: Option<String>,
    pub object_kind: String,
    pub project_id: u64,
    pub project: Project,
    pub push_options: PushOptions,
    pub r#ref: String,
    pub repository: RepositoryInfo,
    pub total_commits_count: u64,
    pub user_avatar: String,
    pub user_email: String,
    pub user_id: u64,
    pub user_name: String,
    pub user_username: String,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PushOptions {
    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

impl ParseTritonRecv for Push {
    fn parse_triton_recv(&self) -> Option<chrono::DateTime<Utc>> {
        if let Ok(triton_recv) = self.triton_recv.parse::<u128>() {
            let seconds = (triton_recv / 1_000_000_000_u128) as i64;
            let nseconds = (triton_recv % 1_000_000_000_u128) as u32;
            if let Some(naive) = DateTime::from_timestamp(seconds, nseconds) {
                return Some(naive);
            }
        }
        None
    }
}
