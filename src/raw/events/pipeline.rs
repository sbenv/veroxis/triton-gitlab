use std::collections::HashMap;

use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use serde_enum_str::{Deserialize_enum_str, Serialize_enum_str};

use crate::parse_triton_recv::ParseTritonRecv;
use crate::raw::types::commit::Commit;
use crate::raw::types::hook_date::HookDate;
use crate::raw::types::project::Project;
use crate::raw::types::runner::Runner;
use crate::raw::types::unknown_value::UnknownValue;
use crate::raw::types::user::User;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Pipeline {
    pub triton_recv: String,
    pub builds: Vec<Build>,
    pub commit: Commit,
    pub merge_request: Option<MergeRequest>,
    pub object_attributes: ObjectAttributes,
    pub object_kind: String,
    pub project: Project,
    pub user: User,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Build {
    pub allow_failure: bool,
    pub artifacts_file: ArtifactsFile,
    pub created_at: HookDate,
    pub duration: Option<f64>,
    pub environment: Option<UnknownValue>,
    pub failure_reason: Option<String>,
    pub finished_at: Option<HookDate>,
    pub id: u64,
    pub manual: bool,
    pub name: String,
    pub queued_duration: Option<f64>,
    pub runner: Option<Runner>,
    pub stage: String,
    pub started_at: Option<HookDate>,
    pub status: String,
    pub user: User,
    pub when: String,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ArtifactsFile {
    pub filename: Option<String>,
    pub size: Option<u64>,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ObjectAttributes {
    pub before_sha: String,
    pub created_at: HookDate,
    pub detailed_status: String,
    pub duration: Option<f64>,
    pub finished_at: Option<HookDate>,
    pub id: u64,
    pub iid: Option<u64>,
    pub queued_duration: Option<f64>,
    pub r#ref: String,
    pub sha: String,
    pub source: String,
    pub stages: Vec<String>,
    pub status: PipelineStep,
    pub tag: bool,
    pub variables: Vec<UnknownValue>,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct MergeRequest {
    pub id: u64,
    pub iid: u64,
    pub merge_status: String,
    pub source_branch: String,
    pub source_project_id: u64,
    pub state: String,
    pub target_branch: String,
    pub target_project_id: u64,
    pub title: String,
    pub url: String,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Deserialize_enum_str, Serialize_enum_str, Clone, PartialEq, Eq, Hash)]
#[serde(rename_all = "snake_case")]
pub enum PipelineStep {
    Created,
    WaitingForResource,
    Pending,
    Running,
    Success,
    Canceled,
    Failed,
    Skipped,

    #[serde(other)]
    Unknown(String),
}

impl ParseTritonRecv for Pipeline {
    fn parse_triton_recv(&self) -> Option<chrono::DateTime<Utc>> {
        if let Ok(triton_recv) = self.triton_recv.parse::<u128>() {
            let seconds = (triton_recv / 1_000_000_000_u128) as i64;
            let nseconds = (triton_recv % 1_000_000_000_u128) as u32;
            if let Some(naive) = DateTime::from_timestamp(seconds, nseconds) {
                return Some(naive);
            }
        }
        None
    }
}
