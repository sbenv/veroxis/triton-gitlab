use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use super::hook_date::HookDate;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Label {
    pub id: u64,
    pub title: String,
    pub color: String,
    pub project_id: Option<u64>,
    pub created_at: HookDate,
    pub updated_at: HookDate,
    pub template: bool,
    pub description: Option<String>,
    pub r#type: String,
    pub group_id: Option<u64>,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}
