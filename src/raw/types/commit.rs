use std::collections::HashMap;
use std::path::PathBuf;

use serde::{Deserialize, Serialize};

use super::author::Author;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Commit {
    pub added: Option<Vec<String>>,
    pub author: Author,
    pub id: String,
    pub message: String,
    pub modified: Option<Vec<PathBuf>>,
    pub removed: Option<Vec<String>>,
    pub timestamp: String,
    pub title: String,
    pub url: String,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}
