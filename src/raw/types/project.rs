use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Project {
    pub avatar_url: Option<String>,
    pub ci_config_path: Option<String>,
    pub default_branch: String,
    pub description: Option<String>,
    pub git_http_url: String,
    pub git_ssh_url: String,
    pub homepage: Option<String>,
    pub http_url: Option<String>,
    pub id: u64,
    pub name: String,
    pub namespace: String,
    pub path_with_namespace: String,
    pub ssh_url: Option<String>,
    pub url: Option<String>,
    pub visibility_level: u64,
    pub web_url: String,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}
