use chrono::{DateTime, Utc};
use serde::de::{Error, Unexpected};
use serde::{Deserialize, Deserializer, Serialize, Serializer};

#[derive(Debug, Clone, Copy)]
pub struct HookDate(pub DateTime<Utc>);

impl Serialize for HookDate {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        self.0.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for HookDate {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let val = String::deserialize(deserializer)?;

        if let Ok(dt) =
            DateTime::parse_from_str(val.as_str(), "%+").map(|dt| dt.with_timezone(&Utc))
        {
            return Ok(HookDate(dt));
        }

        let error_unsupported_format = D::Error::invalid_value(
            Unexpected::Other("hook date"),
            &format!("unsupported format: [{val}]").as_str(),
        );
        Err(error_unsupported_format)
    }
}

impl AsRef<DateTime<Utc>> for HookDate {
    fn as_ref(&self) -> &DateTime<Utc> {
        &self.0
    }
}

#[cfg(test)]
mod tests {
    use chrono::NaiveDate;

    use super::*;

    #[test]
    fn test_iso8601() {
        let input = "\"2023-09-30 18:03:42 UTC\"";
        let expected_date = NaiveDate::from_ymd_opt(2023, 9, 30)
            .unwrap()
            .and_hms_opt(18, 3, 42)
            .unwrap()
            .and_utc();
        let hook_date = serde_json::from_str::<HookDate>(input).unwrap();
        let date = hook_date.0;
        assert_eq!(expected_date, date);
    }

    #[test]
    fn test_rfc3339() {
        let input = "\"2023-09-30T18:03:42UTC\"";
        let expected_date = NaiveDate::from_ymd_opt(2023, 9, 30)
            .unwrap()
            .and_hms_opt(18, 3, 42)
            .unwrap()
            .and_utc();
        let hook_date = serde_json::from_str::<HookDate>(input).unwrap();
        let date = hook_date.0;
        assert_eq!(expected_date, date);
    }
}
