use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RepositoryInfo {
    pub description: Option<String>,
    pub git_http_url: String,
    pub git_ssh_url: String,
    pub homepage: String,
    pub name: String,
    pub url: String,
    pub visibility_level: u64,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}
