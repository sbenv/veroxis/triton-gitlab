/// dynamic union type over any json
///
/// used for fields which are very dynamic
pub type GenericValue = serde_json::Value;
