use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Repository {
    pub description: Option<String>,
    pub homepage: String,
    pub name: String,
    pub url: String,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}
