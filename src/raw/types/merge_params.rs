use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct MergeParams {
    pub auto_merge_strategy: Option<String>,
    pub commit_message: Option<String>,
    /// observed multiple types: `true`, `false` and `"1"`
    pub force_remove_source_branch: Option<Value>,
    pub sha: Option<String>,
    pub should_remove_source_branch: Option<bool>,
    pub squash_commit_message: Option<String>,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}
