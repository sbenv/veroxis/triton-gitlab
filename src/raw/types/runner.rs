use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Runner {
    pub active: bool,
    pub description: String,
    pub id: u64,
    pub is_shared: bool,
    pub runner_type: String,
    pub tags: Vec<String>,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}
