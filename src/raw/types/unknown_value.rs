/// for fields which are not known yet
///
/// by using `UnknownValue` on fields which are not known because e.g. they have always been `null` it is marked that this value has to be defined if possible
pub type UnknownValue = serde_json::Value;
