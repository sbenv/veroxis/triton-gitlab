use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Wiki {
    web_url: String,
    git_ssh_url: String,
    git_http_url: String,
    path_with_namespace: String,
    default_branch: String,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}
