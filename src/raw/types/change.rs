use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use super::generic_value::GenericValue;

pub type Changes = HashMap<String, Change<GenericValue>>;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Change<T> {
    pub previous: Option<T>,
    pub current: Option<T>,

    #[serde(default)]
    #[serde(flatten)]
    pub _unknown: HashMap<String, serde_json::Value>,
}
