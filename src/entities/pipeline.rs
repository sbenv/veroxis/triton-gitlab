use chrono::{DateTime, TimeDelta, Utc};
use serde::{Deserialize, Serialize};
use serde_enum_str::{Deserialize_enum_str, Serialize_enum_str};

use super::job::Job;
use crate::raw::events::pipeline::PipelineStep;

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct Pipeline {
    pub id: u64,
    pub state: PipelineState,
    pub created_at: Option<DateTime<Utc>>,
    pub finished_at: Option<DateTime<Utc>>,
    pub jobs: Vec<Job>,
}

#[derive(Debug, Deserialize_enum_str, Serialize_enum_str, Clone, Default)]
#[serde(rename_all = "snake_case")]
pub enum PipelineState {
    #[default]
    Created,
    WaitingForResource,
    Pending,
    Running,
    Success,
    Canceled,
    Failed,
    Skipped,

    #[serde(other)]
    Unknown(String),
}

impl Pipeline {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn is_active(&self) -> bool {
        // the pipeline is not active if any of the "finished" states has been reached
        use PipelineState::*;
        match self.state {
            Success | Canceled | Failed | Skipped => return false,
            _ => {}
        }
        // the pipeline will be treated as not active if it is older than 24 hours
        if let Some(created_at) = self.created_at {
            let max_duration = TimeDelta::try_days(1).expect("1 day always works");
            let now = Utc::now();
            let oldest_allowed_created_at = now - max_duration;
            if created_at < oldest_allowed_created_at {
                return false;
            }
        }
        true
    }
}

impl From<PipelineStep> for PipelineState {
    fn from(other: PipelineStep) -> Self {
        use PipelineState::*;
        match other {
            PipelineStep::Created => Created,
            PipelineStep::WaitingForResource => WaitingForResource,
            PipelineStep::Pending => Pending,
            PipelineStep::Running => Running,
            PipelineStep::Success => Success,
            PipelineStep::Canceled => Canceled,
            PipelineStep::Failed => Failed,
            PipelineStep::Skipped => Skipped,
            PipelineStep::Unknown(unknown) => Unknown(unknown),
        }
    }
}
