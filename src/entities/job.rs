use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use serde_enum_str::{Deserialize_enum_str, Serialize_enum_str};

use crate::raw::events::build::BuildStep;

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct Job {
    pub id: u64,
    pub name: String,
    pub state: JobState,
    pub repository: String,
    pub r#ref: String,
    pub commit_author: String,
    pub commit_message: String,
    pub runner: Option<String>,
    pub created_at: Option<DateTime<Utc>>,
    pub started_at: Option<DateTime<Utc>>,
    pub finished_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Deserialize_enum_str, Serialize_enum_str, Clone, Default, Eq, PartialEq)]
#[serde(rename_all = "snake_case")]
pub enum JobState {
    #[default]
    Created,
    WaitingForResource,
    Pending,
    Running,
    Canceled,
    Failed,
    Success,

    #[serde(other)]
    Unknown(String),
}

impl JobState {
    pub fn all_job_states() -> Vec<JobState> {
        vec![
            JobState::Created,
            JobState::WaitingForResource,
            JobState::Pending,
            JobState::Running,
            JobState::Canceled,
            JobState::Failed,
            JobState::Success,
            JobState::Unknown(String::new()),
        ]
    }
}

impl Job {
    pub fn new() -> Self {
        Self::default()
    }
}

impl From<BuildStep> for JobState {
    fn from(build_step: BuildStep) -> Self {
        use JobState::*;
        match build_step {
            BuildStep::Created => Created,
            BuildStep::WaitingForResource => WaitingForResource,
            BuildStep::Pending => Pending,
            BuildStep::Running => Running,
            BuildStep::Canceled => Canceled,
            BuildStep::Failed => Failed,
            BuildStep::Success => Success,
            BuildStep::Unknown(unknown) => Unknown(unknown),
        }
    }
}
