use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};

use super::job::{Job, JobState};
use super::pipeline::{Pipeline, PipelineState};
use crate::raw::events;

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct Project {
    /// A map of all `Build` objects indexed by their `build_id`. The `build_id` is read from Gitlab.
    ///
    /// Gitlab manages `Build` objects in a __StateMachine__ which is reflected by `BuildStep`.
    pub builds: BTreeMap<u64, Vec<events::build::Build>>,

    /// A map of all `Pipeline` objects indexed by their `pipeline_id`. The `pipeline_id` is read from Gitlab.
    ///
    /// Gitlab manages `Pipeline` objects in a __StateMachine__ which is reflected by `PipelineStep`.
    ///
    /// The inner `HashMap` stores each `Pipeline` indexed by its corresponding `PipelineStep`.
    pub pipelines: BTreeMap<u64, Vec<events::pipeline::Pipeline>>,

    /// A map of a list of all `MergeRequest` objects indexed by their `merge_request_id`. The `merge_request_id` is read from Gitlab.
    ///
    /// When a `MergeRequest` is changed in any way an update is sent. The changes are stored in a list.
    pub merge_requests: BTreeMap<u64, Vec<events::merge_request::MergeRequest>>,

    /// A map of a list of all `Push` objects indexed by their `project_id`. The `project_id` is read from Gitlab.
    pub pushs: BTreeMap<u64, Vec<events::push::Push>>,

    pub issues: BTreeMap<u64, Vec<events::issue::Issue>>,

    pub notes: BTreeMap<u64, Vec<events::note::Note>>,
}

impl Project {
    pub fn new() -> Self {
        Self::default()
    }

    /// search for an entry containing the projects url
    ///
    /// if all collections are empty this might be `None`
    pub fn url(&self) -> Option<String> {
        for (_, build) in self.builds.iter() {
            if let Some(build) = build.iter().next() {
                return Some(build.repository.homepage.to_owned());
            }
        }
        for (_, pipeline) in self.pipelines.iter() {
            if let Some(pipeline) = pipeline.iter().next() {
                return Some(pipeline.project.web_url.to_owned());
            }
        }
        for (_, merge_requests) in self.merge_requests.iter() {
            if let Some(merge_request) = merge_requests.iter().next() {
                return Some(merge_request.project.web_url.to_owned());
            }
        }
        for (_, pushs) in self.pushs.iter() {
            if let Some(push) = pushs.iter().next() {
                return Some(push.project.web_url.to_owned());
            }
        }
        for (_, issues) in self.issues.iter() {
            if let Some(issue) = issues.iter().next() {
                return Some(issue.project.web_url.to_owned());
            }
        }
        for (_, notes) in self.notes.iter() {
            if let Some(note) = notes.iter().next() {
                return Some(note.project.web_url.to_owned());
            }
        }
        None
    }

    /// search for an entry containing the projects path in gitlab
    ///
    /// if all collections are empty this might be `None`
    pub fn path_with_namespace(&self) -> Option<String> {
        for (_, pipeline) in self.pipelines.iter() {
            if let Some(pipeline) = pipeline.iter().next() {
                return Some(pipeline.project.path_with_namespace.to_owned());
            }
        }
        for (_, merge_requests) in self.merge_requests.iter() {
            if let Some(merge_request) = merge_requests.iter().next() {
                return Some(merge_request.project.path_with_namespace.to_owned());
            }
        }
        for (_, pushs) in self.pushs.iter() {
            if let Some(push) = pushs.iter().next() {
                return Some(push.project.path_with_namespace.to_owned());
            }
        }
        for (_, issues) in self.issues.iter() {
            if let Some(issue) = issues.iter().next() {
                return Some(issue.project.path_with_namespace.to_owned());
            }
        }
        for (_, notes) in self.notes.iter() {
            if let Some(note) = notes.iter().next() {
                return Some(note.project.path_with_namespace.to_owned());
            }
        }
        None
    }

    pub fn pipelines(&self) -> BTreeMap<u64, Vec<Pipeline>> {
        let mut pipelines: BTreeMap<u64, Vec<Pipeline>> = BTreeMap::new();
        for (pipeline_id, event_pipelines) in self.pipelines.iter() {
            for event_pipeline in event_pipelines.iter() {
                let mut pipeline = Pipeline::new();
                pipeline.id = *pipeline_id;
                pipeline.state =
                    PipelineState::from(event_pipeline.object_attributes.status.clone());
                pipeline.created_at = Some(event_pipeline.object_attributes.created_at.0);
                if let Some(finished_at) = event_pipeline.object_attributes.finished_at {
                    pipeline.finished_at = Some(finished_at.0);
                }
                let build_ids = event_pipeline
                    .builds
                    .iter()
                    .map(|build| build.id)
                    .collect::<Vec<u64>>();
                for build_id in build_ids.iter() {
                    if let Some(builds) = self.builds.get(build_id) {
                        for build in builds.iter() {
                            let mut job = Job::new();
                            job.id = build.build_id;
                            job.name = build.build_name.clone();
                            job.state = JobState::from(build.build_status.clone());
                            job.repository = build.repository.homepage.clone();
                            job.r#ref = build.r#ref.clone();
                            job.commit_author = build.commit.author_name.clone();
                            job.commit_message = build.commit.message.clone();
                            if let Some(runner) = &build.runner {
                                job.runner = Some(runner.description.clone());
                            }
                            job.created_at = Some(build.build_created_at.0);
                            if let Some(started_at) = &build.build_started_at {
                                job.started_at = Some(started_at.0);
                            }
                            if let Some(finished_at) = &build.build_finished_at {
                                job.finished_at = Some(finished_at.0);
                            }
                            pipeline.jobs.push(job);
                        }
                    }
                }
                let pipeline_list = pipelines.entry(*pipeline_id).or_default();
                pipeline_list.push(pipeline);
            }
        }
        pipelines
    }

    pub fn push_event_build(&mut self, webhook_build: events::build::Build) {
        let build_id = webhook_build.build_id;
        let builds = self.builds.entry(build_id).or_default();
        if builds
            .iter()
            .filter(|existing_build| existing_build.triton_recv == webhook_build.triton_recv)
            .count()
            == 0
        {
            builds.push(webhook_build);
        }
    }

    pub fn push_event_pipeline(&mut self, webhook_pipeline: events::pipeline::Pipeline) {
        let pipeline_id = webhook_pipeline.object_attributes.id;
        let pipeline_steps = self.pipelines.entry(pipeline_id).or_default();
        if pipeline_steps
            .iter()
            .filter(|existing_pipeline| {
                existing_pipeline.triton_recv == webhook_pipeline.triton_recv
            })
            .count()
            == 0
        {
            pipeline_steps.push(webhook_pipeline);
        }
    }

    pub fn push_event_merge_request(
        &mut self,
        webhook_merge_request: events::merge_request::MergeRequest,
    ) {
        let merge_request_id = webhook_merge_request.object_attributes.id;
        let merge_request_list = self.merge_requests.entry(merge_request_id).or_default();
        if merge_request_list
            .iter()
            .filter(|existing_mr| existing_mr.triton_recv == webhook_merge_request.triton_recv)
            .count()
            == 0
        {
            merge_request_list.push(webhook_merge_request);
        }
    }

    pub fn push_event_push(&mut self, webhook_push: events::push::Push) {
        let project_id = webhook_push.project_id;
        let push_list = self.pushs.entry(project_id).or_default();
        if push_list
            .iter()
            .filter(|existing_push| existing_push.triton_recv == webhook_push.triton_recv)
            .count()
            == 0
        {
            push_list.push(webhook_push);
        }
    }

    pub fn push_event_issue(&mut self, webhook_issue: events::issue::Issue) {
        let issue_id = webhook_issue.object_attributes.id;
        let issue_update_list = self.issues.entry(issue_id).or_default();
        if issue_update_list
            .iter()
            .filter(|existing_issue_update| {
                existing_issue_update.triton_recv == webhook_issue.triton_recv
            })
            .count()
            == 0
        {
            issue_update_list.push(webhook_issue);
        }
    }

    pub fn push_event_note(&mut self, webhook_note: events::note::Note) {
        let note_id = webhook_note.object_attributes.id;
        let note_update_list = self.notes.entry(note_id).or_default();
        if note_update_list
            .iter()
            .filter(|existing_note| existing_note.triton_recv == webhook_note.triton_recv)
            .count()
            == 0
        {
            note_update_list.push(webhook_note);
        }
    }
}
