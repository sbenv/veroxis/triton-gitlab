use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};

use super::project::Project;
use crate::webhook::Webhook;

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct ProjectStore {
    pub projects: BTreeMap<u64, Project>,
}

impl ProjectStore {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn push_event(&mut self, event: Webhook) {
        match event {
            Webhook::Build(webhook_build) => {
                let project_id = webhook_build.project_id;
                let project = self.projects.entry(project_id).or_default();
                project.push_event_build(*webhook_build);
            }
            Webhook::Deployment(_) => {}
            Webhook::FeatureFlag(_) => {}
            Webhook::Issue(webhook_issue) => {
                let project_id = webhook_issue.project.id;
                let project = self.projects.entry(project_id).or_default();
                project.push_event_issue(*webhook_issue);
            }
            Webhook::MergeRequest(webhook_merge_request) => {
                let project_id = webhook_merge_request.project.id;
                let project = self.projects.entry(project_id).or_default();
                project.push_event_merge_request(*webhook_merge_request);
            }
            Webhook::Note(webhook_note) => {
                let project_id = webhook_note.project.id;
                let project = self.projects.entry(project_id).or_default();
                project.push_event_note(*webhook_note);
            }
            Webhook::Pipeline(webhook_pipeline) => {
                let project_id = webhook_pipeline.project.id;
                let project = self.projects.entry(project_id).or_default();
                project.push_event_pipeline(*webhook_pipeline);
            }
            Webhook::Push(webhook_push) => {
                let project_id = webhook_push.project.id;
                let project = self.projects.entry(project_id).or_default();
                project.push_event_push(*webhook_push);
            }
            Webhook::Release(_) => {}
            Webhook::TagPush(webhook_push) => {
                let project_id = webhook_push.project.id;
                let project = self.projects.entry(project_id).or_default();
                project.push_event_push(*webhook_push);
            }
            Webhook::WikiPage(_) => {}
        }
    }
}
