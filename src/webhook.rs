use chrono::Utc;
use serde::{Deserialize, Serialize};
use serde_json::json;
use thiserror::Error;

use crate::raw::events;

#[derive(Debug, Error, Serialize, Deserialize, Clone)]
pub enum WebhookParseError {
    #[error("SerdeJson: `{error}`: `{json}`")]
    SerdeJson {
        error: String,
        json: String,
        value: serde_json::Value,
    },

    #[error("UnknownValue: `{0}`")]
    UnknownValue(serde_json::Value),

    #[error("InvalidObjectKind: `{0}`")]
    InvalidObjectKind(serde_json::Value),
}

impl WebhookParseError {
    pub fn serde_json_error(err: serde_json::Error, value: serde_json::Value) -> Self {
        Self::SerdeJson {
            error: err.to_string(),
            json: value.to_string(),
            value,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub enum Webhook {
    Build(Box<events::build::Build>),
    Deployment(Box<events::deployment::Deployment>),
    FeatureFlag(Box<events::feature_flag::FeatureFlag>),
    Issue(Box<events::issue::Issue>),
    MergeRequest(Box<events::merge_request::MergeRequest>),
    Note(Box<events::note::Note>),
    Pipeline(Box<events::pipeline::Pipeline>),
    Push(Box<events::push::Push>),
    Release(Box<events::release::Release>),
    TagPush(Box<events::push::Push>),
    WikiPage(Box<events::wiki_page::WikiPage>),
}

impl TryFrom<&serde_json::Value> for Webhook {
    type Error = WebhookParseError;

    fn try_from(value: &serde_json::Value) -> Result<Self, Self::Error> {
        match value["object_kind"].as_str() {
            Some(kind) => match kind {
                "push" | "tag_push" => Ok(Webhook::Push(Box::new(
                    serde_json::from_value::<events::push::Push>(value.to_owned()).map_err(
                        |err| WebhookParseError::serde_json_error(err, value.to_owned()),
                    )?,
                ))),
                "issue" => Ok(Webhook::Issue(Box::new(
                    serde_json::from_value::<events::issue::Issue>(value.to_owned()).map_err(
                        |err| WebhookParseError::serde_json_error(err, value.to_owned()),
                    )?,
                ))),
                "note" => Ok(Webhook::Note(Box::new(
                    serde_json::from_value::<events::note::Note>(value.to_owned()).map_err(
                        |err| WebhookParseError::serde_json_error(err, value.to_owned()),
                    )?,
                ))),
                "merge_request" => Ok(Webhook::MergeRequest(Box::new(
                    serde_json::from_value::<events::merge_request::MergeRequest>(value.to_owned())
                        .map_err(|err| {
                            WebhookParseError::serde_json_error(err, value.to_owned())
                        })?,
                ))),
                "wiki_page" => Ok(Webhook::WikiPage(Box::new(
                    serde_json::from_value::<events::wiki_page::WikiPage>(value.to_owned())
                        .map_err(|err| {
                            WebhookParseError::serde_json_error(err, value.to_owned())
                        })?,
                ))),
                "pipeline" => Ok(Webhook::Pipeline(Box::new(
                    serde_json::from_value::<events::pipeline::Pipeline>(value.to_owned())
                        .map_err(|err| {
                            WebhookParseError::serde_json_error(err, value.to_owned())
                        })?,
                ))),
                "build" => Ok(Webhook::Build(Box::new(
                    serde_json::from_value::<events::build::Build>(value.to_owned()).map_err(
                        |err| WebhookParseError::serde_json_error(err, value.to_owned()),
                    )?,
                ))),
                "deployment" => Ok(Webhook::Deployment(Box::new(
                    serde_json::from_value::<events::deployment::Deployment>(value.to_owned())
                        .map_err(|err| {
                            WebhookParseError::serde_json_error(err, value.to_owned())
                        })?,
                ))),
                "feature_flag" => Ok(Webhook::FeatureFlag(Box::new(
                    serde_json::from_value::<events::feature_flag::FeatureFlag>(value.to_owned())
                        .map_err(|err| WebhookParseError::serde_json_error(err, value.to_owned()))?,
                ))),
                "release" => Ok(Webhook::Release(Box::new(
                    serde_json::from_value::<events::release::Release>(value.to_owned()).map_err(
                        |err| WebhookParseError::serde_json_error(err, value.to_owned()),
                    )?,
                ))),
                _ => {
                    let mut value_owned = value.clone();
                    if let Some(value_object) = value_owned.as_object_mut() {
                        value_object.insert("triton_recv".to_string(), json!(Utc::now()));
                    }

                    Err(WebhookParseError::UnknownValue(value_owned))
                }
            },
            None => {
                let mut value_owned = value.clone();
                if let Some(value_object) = value_owned.as_object_mut() {
                    value_object.insert("triton_recv".to_string(), json!(Utc::now()));
                }

                Err(WebhookParseError::InvalidObjectKind(value_owned))
            }
        }
    }
}
