use chrono::prelude::*;
use serde_json::Value;

pub trait ParseTritonRecv {
    fn parse_triton_recv(&self) -> Option<chrono::DateTime<Utc>>;
}

impl ParseTritonRecv for Value {
    fn parse_triton_recv(&self) -> Option<chrono::DateTime<Utc>> {
        if let Some(triton_recv_string) = self["triton_recv"].as_str() {
            if let Ok(triton_recv) = triton_recv_string.parse::<u128>() {
                let seconds = (triton_recv / 1_000_000_000_u128) as i64;
                let nseconds = (triton_recv % 1_000_000_000_u128) as u32;
                if let Some(naive) = DateTime::from_timestamp(seconds, nseconds) {
                    return Some(naive);
                }
            }
        }
        None
    }
}
